// Variables
  const int sensorPin = 27;
  float reading = 0;
  float tare = 0;

// Variables for Averaging
  const int totalSamples = 10;
  int index_avg = 0;
  int value = 0;
  float sum = 0;
  int readings[totalSamples]; // create an empty array of size totalSamples

// Variables for Low Pass
  float filteredOutputPrevious = 0;
  const float RC = 0.159;
  const float dT = 0.01; // time in seconds
  const float C1 = dT/(RC+dT); // coefficient 1
  const float C2 = RC/(RC+dT); // coefficient 2

void setup() {
    Serial.begin(115200);                                     // initialize serial communication with computer:
    analogReadResolution(10);                               // Here we can set the number of bits used by our ADC 
    
   // Now for our tare, we can use our low pass filter several times to get more accuracy
    filteredOutputPrevious = analogRead(sensorPin);         // First let's initialize the previous value with a single new reading
    for (int i = 0; i < 10; i++) {                          // Next let's repeat the measurement a few times to make sure we are getting a nice filtered output
        tare = lowPassRead();
    }
}

void loop() {
    float reading = analogRead(sensorPin)  - tare;
    Serial.print(reading);
    Serial.print(", ");
    float filtered_reading = batchAverageRead() - tare;             // Batch average reading
    //float filtered_reading = movingAverageRead()  - tare;           // Moving average read
    //float filtered_reading = lowPassRead()  - tare;                   // Low pass read
    Serial.println(filtered_reading);                                 // send our reading over USB to the computer
    delay(dT*1000);                                                   // delay between reads in miliseconds (necessary for low pass)
}

// Batch Averaged sensor reading ----------------------------------------------------
float batchAverageRead(){
    sum = 0;
    for (int i = 0; i < totalSamples; i++){
        sum = sum + analogRead(sensorPin);                            // sum totalSamples readings
        delay(dT*1000);                                                    // wait for every reading! This is bad :(
    }
    return sum / totalSamples;
}

// Moving Average sensor reading ----------------------------------------------------
float movingAverageRead(){
    sum = sum - readings[index_avg];                            // Remove the oldest entry from the sum
    value = analogRead(sensorPin);                              // Read the next sensor value
    readings[index_avg] = value;                                // Add the newest reading to the window
    sum = sum + value;                                          // Add the newest reading to the sum
    index_avg = (index_avg+1)%totalSamples;                     // Increment the index, and wrap to 0 if it exceeds the window size
    return sum/(float(totalSamples));                           // Divide the sum of the window by the window size for the result
}

// Low Pass Filter sensor reading -------------------------------------------------
float lowPassRead(){
    float filteredOutput = analogRead(sensorPin)*(C1) + filteredOutputPrevious*(C2);  // All we need to do here is to measure once and do some fast multiplication with our coefficients! This is great :)
    filteredOutputPrevious = filteredOutput;                                          // And store our output for next time
    return filteredOutput;
}
