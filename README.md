# Input Devices Recitation

This recitation aims to introduce the basics of working with inputs on Arduino embeded devices. Slides are hosted [here](https://gitlab.cba.mit.edu/classes/863.22/site/-/blob/master/doc/input_devices/Input%20Devices%202022.pdf).

### Example Code

1) [inputDevices_1_basic.ino](https://gitlab.cba.mit.edu/classes/863.22/site/-/blob/master/doc/input_devices/inputDevices_1_basic.ino) is a simple sketch introducing analogRead().

2) [inputDevices_2_gauss.ino](https://gitlab.cba.mit.edu/classes/863.22/site/-/blob/master/doc/input_devices/inputDevices_1_basic.ino) adds a tare, ADC resolution, and conversion to Gauss.

3) [inputDevices_3_filters.ino](https://gitlab.cba.mit.edu/classes/863.22/site/-/blob/master/doc/input_devices/inputDevices_1_basic.ino) shows various filtering approaches, including batch averaging, moving averaging, and low pass filters.

4) [inputDevices_4_linearization.ino](https://gitlab.cba.mit.edu/classes/863.22/site/-/blob/master/doc/input_devices/inputDevices_1_basic.ino) shows a simple lookup table implementation with linearization.
