const int sensorPin = 27;                 // Pin where our sensor's output is connected

void setup() {
    Serial.begin(115200);                   // Initialize serial communication with the computer
}

void loop() {
    int reading = analogRead(sensorPin);  // Read the analog voltage at sensorPin and store it as an int called reading
    Serial.println(reading);              // Send that reading over USB to our computer
    delay(10);                            // Wait time in ms before next reading
}
