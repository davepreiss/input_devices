// Variables
const int sensorPin = 27;
float reading = 0;
float tare = 0;                                            // initializing our tare
const int ADC_res = 10;                                    // desired ADC resolution
const float sens = 0.005;                                  // V/G, taken from datasheet
const float conversion_factor = 3.3/(pow(2,ADC_res))/sens; // [V/ADC] / [V/G] gives us Gauss/ADC

void setup() {
    Serial.begin(115200);
    analogReadResolution(ADC_res);                         // Here we can set the number of bits used by our ADC 
    tare = analogRead(sensorPin);                          // Take one reading to use as our tare value
}

void loop() {
    float reading = (analogRead(sensorPin) - tare)*conversion_factor; // now we will subtract by our tare, and multiply by our conversion factor
    Serial.print(reading);
    Serial.println(" Gauss");
    delay(10);
}
