// Variables
const int sensorPin = 27;
float reading = 0;
float tare = 0;

// Variables for Low Pass
float filteredOutputPrevious = 0;
const float RC = 0.159;
const float dT = 0.01; // time in seconds
const float C1 = dT/(RC+dT); // coefficient 1
const float C2 = RC/(RC+dT); // coefficient 2

// Variables for linearization 
const int interp_samples = 10;
float interp_inputs[interp_samples]  = {296, 200.3, 139.5, 100.8, 75, 57, 44.05, 35, 28, 23};   // Raw ADC readings
float interp_outputs[interp_samples] = {0, 2, 4, 6, 8, 10, 12, 14, 16, 18};                     // Associated linear distances

void setup() {
    Serial.begin(115200);                                     // Initialize serial communication with computer:
   // Now for our tare, we can use our low pass filter several times to get more accuracy
    filteredOutputPrevious = analogRead(sensorPin);         // First let's initialize the previous value with a single new reading
    for (int i = 0; i < 10; i++) {                          // Next let's repeat the measurement a few times to make sure we are getting a nice filtered output
        tare = lowPassRead();
    }
}

void loop() {
    float filtered_reading = lowPassRead()  - tare;         // we will always subtract the tare from our readings
    linearized(filtered_reading);                           // send our reading over USB to the computer
    delay(dT*1000);                                         // delay between reads in miliseconds
}

// Low Pass Filter sensor reading -------------------------------------------------
float lowPassRead(){
    float filteredOutput = analogRead(sensorPin)*(C1) + filteredOutputPrevious*(C2);
    filteredOutputPrevious = filteredOutput;
    return filteredOutput;
}

// Linearization ------------------------------------------------------------------
float linearized(float reading){
    float linear_output = reading;                                                                                  // Print our raw output for reference first
    Serial.print("Raw reading: ");
    Serial.print(reading);
    if (reading <= interp_inputs[0] && reading >= interp_inputs[interp_samples-1]){                                 // If we are reading within the interpolation lookup range
        for (int i = 0; i <= interp_samples; i++){
            if (reading > interp_inputs[i]){                                                                        // Find our exact location by iterating through the table and checking each time
                float scaling_factor = abs((reading - interp_inputs[i]) / (interp_inputs[i] - interp_inputs[i-1])); // Calculate our percentage between two interpolation points
                linear_output = (interp_outputs[i-1] - interp_outputs[i])*scaling_factor + interp_outputs[i];       // Apply that scaling factor to the associated conversion points
                Serial.print(" Linearized reading: ");
                Serial.print(linear_output);
                Serial.println(" mm");
                break;
            }
        }
    }
    else {
        Serial.println(" Outside of interpolation range");
    }
    return linear_output;
}
